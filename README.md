# README #

You find the following structure

├──data

├──solutions

├──demo_scripts

Datasets
========================
Data used in tutorials can be found in the subdirectory

├──data/

Solutions
========================
├──solutions/

contains exercise python scripts with solutions for the exercises
Please note that that there are multiple solutions for a problem
Only one is given in this repository


Demo scripts
========================
├──demo_scripts/

This directory contains scripts demonstrated in class
