#----------------------------------------#

#----------------------------------------#
"""
Question 01
Write a method which can calculate square value of number

Hints:
Using the ** operator
"""

##Solution:
def square(num)
    return num ** 2

print("------Q1-------")
print(square(2))
print(square(3))
#----------------------------------------#

#----------------------------------------#
"""
Question 02
Define a function which can compute the sum of two numbers.

Hints:
Define a function with two numbers as arguments. You can compute the sum in the function and return the value.
"""
##Solution
def SumFunction(number1, number2):
	return number1+number2

print("------Q2-------")
print(SumFunction(1,2))

#----------------------------------------#

#----------------------------------------#
"""
Question 03
Define a function that can convert a integer into a string and print it in console.

Hints:

Use str() to convert a number to string.
"""
##Solution
def print_value(n):
	print(str(n))
print("------Q3-------")
print_value(3)	

#----------------------------------------#

#----------------------------------------#
"""
Question 04
Define a function that can convert an ascii character into an integer and print it in console.

Hints:

Use ord() to convert an ascii character to an integer
"""
##Solution
def print_ord_value(a):
	print(ord(a))
	
print("------Q4-------")
print_ord_value('Z')	

#----------------------------------------#

#----------------------------------------#
"""
Question 05
Define a function that can receive two integral numbers in string form
and compute their sum and then print it in console.

Hints:

Use int() to convert a string to integer.
"""
##Solution
def print_sum_value(s1,s2):
	print(int(s1)+int(s2))

print("------Q5-------")
print_sum_value("4","2") #6

#----------------------------------------#

#----------------------------------------#

"""
Question 06

Define a function that can accept two strings as input and
print the string with maximum length in console.
If two strings have the same length, then the function should
print all strings line by line.

Hints:

Use len() function to get the length of a string

"""
##Solution
def printValue(s1,s2):
	len1 = len(s1)
	len2 = len(s2)
	if len1>len2:
	    print(s1)
	elif len2>len1:
	    print(s2)
	else:
	    print(s1)
	    print(s2)
		
print("------Q6-------")
printValue("good","morning")
printValue("see","you")
printValue("only","longest")

#----------------------------------------#

#----------------------------------------#
"""
Question 07
Define a function which can generate and print a list where the values
are square of numbers between 1 and 20 (both included).


Hints:

Use ** operator to get power of a number.
Use range() for loops.
Use list.append() to add values into a list.

expected outcome:
[1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400]
"""
##Solution
def printList():
    l=[]
    for i in range(1,21):
        l.append(i**2)
    print(l)
		
		
print("------Q7-------")
printList()

#----------------------------------------#

#----------------------------------------#
"""
Question 08
Define a function which generates a list where the values are square
of numbers between 1 and 20 (both included).
Then the function needs to print all values except
the first 5 elements in the list.

Hints:

Use ** operator to get power of a number.
Use range() for loops.
Use list.append() to add values into a list.
Use [n1:n2] to slice a list

expected outcome: [36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400]
"""
      
##Solution
def printList():
    li=[]
    for i in range(1,21):
        li.append(i**2)
    print(li[5:])
		
print("------Q8-------")
printList()
#----------------------------------------#
    
#----------------------------------------#
"""
Question 09
Define a function which reverse the sequence string in reverse order


Hints:

first transform the string to a list
use pydoc3 list to find the right reverse method
use ''.join() to convert to string again
"""

##Solution     
def reverse(s):
    letters = list(s)
    letters.reverse()
    return ''.join(letters)

print("------Q9-------")
sequence = "GTGGAAGTTCTTAGGGCATGGCAAAGAGTCAGAATTTGAC"
print(reverse(sequence))
#----------------------------------------#

#----------------------------------------#
"""
Question:
Define a function that reads the file SEQUENCE (see blackboard).
This file contains a single string which needed to be reversed and 
printed


Hints:

use reverse function from previous question
use with open statement to open the file
"""

##Solution     
def reverse(s):
    letters = list(s)
    letters.reverse()
    return ''.join(letters)

print("------Q10------")
with open('SEQUENCE', 'r') as f:
    sequence = f.read()
    sequence = sequence.strip()
print(reverse(sequence))
#----------------------------------------#

#----------------------------------------#
"""
Question:
Define a function that reads the file SEQUENCE (see blackboard).
This file contains a single string which needed to be reversed.
write the reversed string to the file REVERSED_SEQUENCE

Hints:

use code from previous question
use with open statement to open the files
"""

##Solution     
def reverse(s):
    letters = list(s)
    letters.reverse()
    return ''.join(letters)

print("------Q11------")
with open('SEQUENCE', 'r') as f, open("REVERSED_SEQUENCE", 'w') as o:
    sequence = f.read()
    sequence = sequence.strip()
    o.write(reverse(sequence))

#----------------------------------------#
    
#----------------------------------------#
"""

Question 12
Write a function to compute 5/0 and use try/except to catch the exceptions.

Hints:

Use try/except to catch exceptions.
"""
      
##Solution:

def throws():
    return 5/0

print("------Q12------")
try:
    throws()
except ZeroDivisionError:
    print("division by zero!")
except Exception as err:
    print('Caught an exception')
finally:
    print('In finally block for cleanup')


#----------------------------------------#


#----------------------------------------#
"""
Question 13
Define a function that reads the file SEQUENCE (see blackboard).
This file contains a single string which needed to be reversed.
write the reversed string to the file REVERSED_SEQUENCE
use exception handling that catches the IOError

Hints:

use code from previous question
use the try, excep, finally construct
"""

##Solution     
def reverse(s):
    letters = list(s)
    letters.reverse()
    return ''.join(letters)


print("------Q13------")
try: 
    with open('SEQUENCE', 'r') as f, open("REVERSED_SEQUENCE", 'w') as o:
          sequence = f.read()
          o.write(reverse(sequence))
except IOError:
    print("fail to open file")
finally:
    print("process ended")

#----------------------------------------#
      
#----------------------------------------#
"""
Question 14:

Assuming that we have some email addresses in the "username@companyname.com" format,
please write program to print the company name of a given email address.
Both user names and company names are composed of letters only.

Example:
If the following email address is given as input to the program:

john@google.com

Then, the output of the program should be:

google


Hints:

Use expressions to find the company
"""
##Solution:

print("------Q14------")
import re
emailAddress = "flen@mac.com"
pat2 = "(\w+)@(\w+)\.(com)"
r2 = re.match(pat2,emailAddress)
print(r2.group(2))


#----------------------------------------#

#----------------------------------------#
"""
Question 15
Define a function that reads the file sample.faa (see blackboard).
substract the DNA string from the file print it to the console

Hints:

use with open statement to open the file
skip the line with ">"
use regex to find dna (try in regex101 first)
"""
##Solution:

print("------Q15------")
import re
with open('sample.faa', 'r') as f:
    for line in f:
        if not line.startswith(">"):
            m = re.search(r"[ATGC]+", line)
            print(m.group())
#----------------------------------------#
            
#----------------------------------------#
"""
Question 16

The Fibonacci Sequence is computed based on the following formula:


f(n)=0 if n=0
f(n)=1 if n=1
f(n)=f(n-1)+f(n-2) if n>1

Please write a program using list comprehension to print the Fibonacci Sequence in comma separated
form with a given n input by console.

Example:
If the following n is given as input to the program:

7

Then, the output of the program should be:

0,1,1,2,3,5,8,13


Hints:
use a for loop
make an empty list and append the Fibonacci Sequence
Use string.join() to join a list of strings.

In case of input data being supplied to the question, it should be assumed to be a console input.
to be used: n=int(raw_input())
"""
##Solution:

def f(n):
    if n == 0: return 0
    elif n == 1: return 1
    else: return f(n-1)+f(n-2)
    
print("------Q16------")
n=7
values = []
for x in range(0, n+1):
    values.append(str(f(x)))
print(",".join(values))


#----------------------------------------#
"""
Question 17

read the file SEQUENCE. print the length of the sequence, the number of the
sequence, the number of 'G's and the position of first "T"

Hints:
use built ins like s.len, s.count, s.index
use "{}".format construct to print

Outcome:
sequence:GTGGAAGTTCTTAGGGCATGGCAAAGAGTCAGAATTTGAC, length: 40
number of G: 13
index of first T: 1
"""
##Solution:
print("------Q17------")
with open('SEQUENCE', 'r') as f:
    seq = f.read()

seq = seq.strip()
print('sequence: {}, length: {}'.format(seq, len(seq)))
print('number of G: {}'.format(seq.count('G')))
print('index of first T: {}'.format(seq.index('T')))

#----------------------------------------#

#----------------------------------------#
"""
Question 18

write a function that checks for valid DNA
if a base is invalid print a message "invalid character: {}"

Hints:
use for loop to check for valid character
use the "".format construct to print the invalid base

Outcome:
invalid character: X
invalid character: Y
invalid character: N
invalid character: R
invalid character: Y
invalid character: W

"""
print("------Q18------")
def check_dna(seq):
    valid_char = 'GATC'
    for base in seq:
        if not base in valid_char:
            print('invalid character: {}'.format(base))

check_dna('XYNACRYWCCTT')

#----------------------------------------#

#----------------------------------------#

"""
Question 19

Find if enzyme XbaI is present in the a sequence and print the position.
in case of KeyError print "did not find the enzyme"
in case of ValueError print "The restriction site {} was not found in sequence {}"


Hints:
use try except construct
use "".format to print

"""
restriction_enzymes = {
        "EcoRI":"GAATTC",
        "BamHI":"GGATCC",
        "XbaI":"TCTAGA",
        "HindIII":"AAGCTT",
        }

def find_enzyme(seq, enzyme):
    try:
        seq.index(restriction_enzymes[enzyme])
    except KeyError as e:
        print("{} ({}): did not find the enzyme {}".format(type(e).__name__, e.args[0], enzyme))
    except ValueError:
        print("The restriction site {} was not found in sequence {}".format(enzyme, seq))
    else:
        return seq.index(restriction_enzymes[enzyme])

print("------Q19------")
dna = "CTGCAGGAACTTCTTCTGGAAGACCTTCTCCTCCTGCAAATAAAACCTCACCCATGAATGCTCACGCAAG"
restriction_enzyme = "XbaI"
x = find_enzyme(dna, restriction_enzyme)
print("restriction site position: {}".format(x))
