#!usr/bin/env python3

"""
loop that compares start with
start_position. Print "too low" if start is less than start_position.
If start equals start_position print "found it!" and exit the loop.
If start is greater then start_position print "oops!" and exit the loop.
Increment start at the end of the loop.
"""

__author__ = "Fenna Feenstra"

start_position = 7
start = 1

loop = True
while loop:
    print("compare {} with {}".format(start, start_position))
    if start == start_position:
        print("found it!")
        loop = False
    elif start < start_position:
        print("too low")
    else:
        print("oops")
    start += 1
 
