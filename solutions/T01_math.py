#!/usr/bin/env python3

def print_outcome(expression, outcome):
    if outcome == 42:
        print(expression, "Answer to the Ultimate Question of Life, the Universe, and Everything")
    else:
        print(expression, outcome)
        

def main():
    print_outcome("     3∗(2+4)∗3=", 3*(2+4)*3)
    print_outcome("         2+4∗5=", 2+4*5)
    print_outcome("          3∗−6=", 3*-6)
    print_outcome("         −3∗−6=", -3*-6)
    print_outcome("       (1/3)∗4=", (1/3)*4)

main()
