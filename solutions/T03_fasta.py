#!/usr/bin/env python3

"""this programme reads fasta file(s) and reports number of nucleotides/amino acids"""


# METADATA VARIABLES
__author__ = "my name"
__version__ = "2017.1"

# IMPORTS
import sys


# LOGICA

def split_file(file):
    header = ''
    seq = ''
    with open(file, 'r') as f:
        for line in f:
            line = line.strip()
            if line.startswith(">"):
                header = line
            else:
                seq += line
    return(header, seq)

def count_ATCG(sequence):
    """count the nucleotides and print the results """
    nucs = {"A":0, "T":0, "C":0, "G":0}
    s = sequence.upper()
    for nucl in nucs.keys():
        nucs[nucl] = s.count(nucl)
    return nucs

    
def count_cg(sequence):
    """count cg percentage"""
    gc = sequence.count('G') + sequence.count('C')
    return (gc / len(sequence))

def count_aminoc_acids(sequence):
    """count the amino acids and print the results """
    return len(sequence)



def determine_type(sequence):
    """distinghuis file types"""
    file_type = ""
    nucs = count_ATCG(sequence)
    if sum(nucs.values()) == len(sequence):
        file_type = 'nucleotide_fasta'
    else:
        file_type = 'protein_fasta'
    return file_type



def main(args):   
    filenames = args[1:]
    for file in filenames:
        [header, sequence] = split_file(file)
        file_type = determine_type(sequence)
        if file_type == 'protein_fasta':
            print('Amino acids:', count_aminoc_acids(sequence))
        if file_type == 'nucleotide_fasta':
            print('ATCG:', count_ATCG(sequence))
            print('CG percentage: {:.2%}'.format(count_cg(sequence)))
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
