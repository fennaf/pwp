#!/usr/bin/env python3

"""
This scripts accepts a DNA strand and prints the original strand
and the reverse complementary strand.

    usage: rev_comp_dna <sequence>

"""

__author__ = "your name"
__version__ = "0.1"


# IMPORTS
import sys
import DNA

# FUNCTIONS
def print_outcome(seq, rev_comp):
    """
    print the sequence and the reverse_complement sequence.
    """
    print("original sequence\n")
    print("{}{line_break}".format(seq, line_break="\n"))
    print("reverse-complement sequence{line_break}".format(line_break="\n"))
    print("{}{line_break}{line_break}".format(rev_comp, line_break="\n"))
    return 0


# MAIN
def main(argv=None):
    """
    The main function calls the other functions and structures code.
    """
    if argv is None:
        argv = sys.argv

    # catch the command line argument
    if len(argv) != 2:
        print("Please provide a sequence")
        sys.exit(0)
    seq = sys.argv[1]

    rev_seq = DNA.reverse(seq)
    rev_comp = DNA.complement(rev_seq)
    print_outcome(seq, rev_comp)
    return 0


# RUN
if __name__ == "__main__":
    sys.exit(main(sys.argv))
