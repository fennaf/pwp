#!/usr/bin/env python3

"""
This is a program that calculates Phredscore and quality base
input: The predicted read base quality ASCII values of a fastq line
for example #>>1A1B3B11BAEFFBECA0B000EEGFCGBFGGHH2DEG
"""

__author__ = 'Fenna Feenstra'
__version__ = '0.1'

#imports
import sys

#globals
ASCII_offset = 33
sequence = input("Enter your sequence here >")


#functions
def PhredScore(char):
    """ this function scores the base by converting the ascii value to a
    number and substract this with the offset """
    # correct this line
    Num_score = ord(char) -33 
    return Num_score

def Base_call_accuracy(numscore):
    """ this function calculates the quality percentage"""
    # correct this with the correct formula
    exponent =(-1*numscore/10)
    Q = 1 - (10**exponent)
    Q = "{0:.2%}".format(Q)
    return Q

2
def main():
    """ this is the main function which calls other functions"""

    #create empty lists
    Phredlist = []
    Accurlist = []
    #for each character in sequence calculate PhredScore and accuracy
    for character in sequence:
        NS = PhredScore(character)
        QS = Base_call_accuracy(NS)
        # append to list
        Phredlist.append(str(NS))
        Accurlist.append(QS)

    #print the lists in one row seperated by |
    print("Phredscore:   " + "  | ".join(Phredlist))
    print("Accuracy  : " + "|".join(Accurlist))

    
#entryppoint
if __name__ == '__main__':
    sys.exit(main())
