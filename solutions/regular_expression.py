import re

'''
Author: Iwan Hidding
Status: Finished
'''
# question 1
# m = re.search(r"GA[ATGC]{3}AC", dna)

# question 2


s = "KIAA1279(dist=6336),SRGN(dist=64753)CCSER2(dist=489626),LINC01519(dist=18527)4FRG2B"
m = re.findall(r"(\w+)\(",s)
print(*m)

#question 3

dna = "ATCGCGYAATTCAC"
if re.search(r"A{2,}|T{2,}|G{2,}|C{2,}", dna):
    print("ambiguous base found!")

#question 4  zit er een 5 in
accessions = ['xkn59438', 'yhdck2', 'eihd39d9', 'chdsye847', 'hedle3455', 'xjhd53e', '45da', 'de37dp']
print("Antwoord 4:")
a = '[]'
for genes in accessions:
    list1 = re.findall(r"\w{,}5{1,}\w{,}", genes)
    [print(item) for item in list1 if item.strip() is not a]

#question 5 d of e
accessions = ['xkn59438', 'yhdck2', 'eihd39d9', 'chdsye847', 'hedle3455', 'xjhd53e', '45da', 'de37dp']
print("Antwoord 5:")
for genes in accessions:
    list1 = re.findall(r"\w{,}[de]{1}\w{,}", genes)
    [print(item) for item in list1 if item.strip() is not a]

#question 6 d en e
print("Antwoord 6:")
for genes in accessions:
    list1 = re.finditer(r"\w{,}de{1}\w{,}", genes)
    for list2 in list1:
        list3 = list2.group()
        print(list3)
#question 7 d en e met 1 letter er tusse
print("Antwoord 7:")
for genes in accessions:
    list1 = re.finditer(r"\w{,}d[a-zA-Z]{1}e{1}\w{,}", genes)
    for list2 in list1:
        list3 = list2.group()
        print(list3)

#question 8 d en e in verschillende volgorde
print("Antwoord 8:")
for genes in accessions:
    list1 = re.finditer(r"\w{,}[de]\w{,}[de]\w{,}", genes)
    for list2 in list1:
        list3 = list2.group()
        print(list3)
#queston 9 start with x or y
print("Antwoord 9:")
for genes in accessions:
    list1 = re.finditer(r"^[xy]\w{,}", genes)
    for list2 in list1:
        list3 = list2.group()
        print(list3)
#question 10 start with x or y and end with e
print("Antwoord 10:")
for genes in accessions:
    list1 = re.finditer(r"^[xy]\w{,}e$", genes)
    for list2 in list1:
        list3 = list2.group()
        print(list3)
#question 11 contain three or more digits in a row
print("Antwoord 11:")
for genes in accessions:
    list1 = re.finditer(r"\w{,}[0-9]{3,}\w{,}", genes)
    for list2 in list1:
        list3 = list2.group()
        print(list3)

#question 12 end with d followed by either a, r or p
print("Antwoord 12:")
for genes in accessions:
    list1 = re.finditer(r"\w{,}d[arp]{1}$", genes)
    for list2 in list1:
        list3 = list2.group()
        print( list3) 
