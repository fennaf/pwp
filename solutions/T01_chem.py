#!/usr/bin/env python3

mass_H = 1.008
mass_O = 15.998
mass_C = 12.011

# a. Water: H2O
print("The mass of a water molecule (H2O) is", 2 * mass_H + mass_O, "amu.")

# b. Ethanol: C2H5OH
print("The mass of an ethanol molecule (C2H5OH) is", 2 * mass_C + 6 * mass_H + mass_O, "amu" )

# c. Glucose: C6H12O6
print("The mass of a glucose molecule (C6H12O6) is", 6 * mass_C + 12 * mass_H + 6 * mass_O, "amu" )

# d. Saccharose: C12H22O11
print("The mass of a saccharose molecule (C12H22O11) is", 12 * mass_C + 22 * mass_H + 11 * mass_O, "amu" )
