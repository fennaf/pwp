#!/usr/bin/env python3

"""
Download a fasta file containing DNA. Read the file line by line.
If the line is a header write the line to a logfile with a time stamp
"""


__author__ = "Fenna Feenstra"

# IMPORT
import sys
import datetime

# FUNCTIONS
def writelog(line):
    """ function that logs the output"""
    d = datetime.datetime.now()
    d = d.strftime("%d-%m-%Y %H:%M:%S")
    with open("log.txt", "w") as o:
        o.write(line)
        o.write("logtime: {}{linebreak}".format(d, linebreak = "\n"))

def fetch_header(fastafile):
    with open(fastafile) as f:
        for line in f:
            if line.startswith(">"):
                return line
    return ""


def main(args):
    """ main function """
    fastafile = args[1]
    line = fetch_header(fastafile)
    writelog(line)
    return 0


if __name__ == "__main__":
    exitcode = main(sys.argv)
    sys.exit(exitcode)
