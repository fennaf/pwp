#!/usr/bin/env python3
"""
This module demonstrate the functions and usage of arguments
    usage: threefunctions.py
    output example: this is one string
                    two 2
                    8
"""

__author__ = 'your name'
__version__ = '0.1'

#imports
import sys
import pydoc

#globals

#functions

def one_argument(a):
    print(a)

def two_arguments(a, b):
    print(a, b)

def multiply(a, b):
    return a*b



def main():
    """
    The main function of the module gets arguments, calls the functions and prints the result
    """
    one_argument("this is one string")
    two_arguments("two", 2)
    y = multiply(2,4)
    print(y)
    return 0

    
#entryppoint
if __name__ == '__main__':
    sys.exit(main())
