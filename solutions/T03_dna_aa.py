#!/usr/bin/env python3

"""
Read the fasta file line by line. Translate the DNA lines to Amino Acids and write them to a new file
"""


__author__ = "Fenna Feenstra"

# imports
import sys

def read_fasta(file):
    header = ''
    seq = ''
    with open(file, 'r') as file_handler:
        for line in file_handler:
            line = line.strip()
            if line.startswith(">"):
                header = line
            else:
                seq += line
    return(header, seq)



def DNA2protein(seq):
    codontable = {
        'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
        'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
        'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
        'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
        'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
        'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
        'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
        'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
        'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
        'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
        'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
        'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
        'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
        'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
        'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
        'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
    }

    protein = ""
    for i in range(0,len(seq),3):
        codon = seq[i:i+3]
        if codon in codontable:
            protein += codontable[codon]
    return protein


def save_file(file_name, string):
    with open(file_name, 'w') as FILE:
        FILE.write(string)

def main(args):
    file_name = args[1]
    [header, seq] = read_fasta(file_name)
    aa = DNA2protein(seq)
    save_file(file_name[0:5]+"_aa"+".fasta",header+"\n"+aa)


if __name__ == "__main__":
    sys.exit(main(sys.argv))
