#!/usr/bin/env python3

"""
programme that Writes a logfile that contains a line with the date and time and a line with a message
"""


__author__ = "Fenna Feenstra"

# IMPORT
import sys
import datetime

# FUNCTIONS
def writelog(message):
    """ function that logs the output"""
    d = datetime.datetime.now()
    d = d.strftime("%d-%m-%Y %H:%M:%S")
    with open("log.txt", "w") as o:
        o.write("{} logtime: {}{linebreak}".format(message, d, linebreak = "\n"))



def main(args):
    """ main function """
    message = args[1]
    writelog(message)
    return 0


if __name__ == "__main__":
    exitcode = main(sys.argv)
    sys.exit(exitcode)
