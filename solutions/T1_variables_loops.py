#!/usr/bin/env python3
""" answers exercise lecture 02 """


#imports
import math #read the pydoc3 math to learn more about math


#Question 1
#change the code to get the correct answer

# a. 3 times 12
a = 3*12
print("answer a should be 36: {}".format(a))

# b. 30 divided by 4
b = 30/4
print("answer b should be 7.5: {}".format(b))

# c. 3 to the power of 3
c = 3**3
print("answer c should be 27: {}".format(c))

# d. 10 divided by three (as float)
d = 10/3
print("answer d should be 3.3333333333333335: {}".format(d))

# e. 10 divided by three (as integer)
e = int(10/3)
print("answer e should be 3: {}".format(e))

# f. the square root of 42
f = math.sqrt(42)
print("answer f should be 6.48074069840786: {}".format(f))

# g. the log2 of 1024
g = math.log2(1024)
print("answer g should be 10: {}".format(g))

# h. the log10 of 1024
h = math.log10(1024)
print("answer h should be 3.010299956639812: {}".format(h))

#Question 2
seq = "GATCGATCGATC"

#write the code that will print
print(seq[4:9])
#CGATCG
print(seq[8:12])
#GATC
print(seq.lower())
#gatcgatcgatc
print(seq[::4])
#GGG
print(4*seq)        
#GATCGATCGATCGATCGATCGATCGATCGATCGATC

#print codonlist
codonlist = []
for i in range(0,len(seq),3):
    codonlist.append(seq[i:i+3])
for codon in codonlist:
    print(codon) 


#Question 3
#Create a script with a dictionary that holds the prices for lab equipment:
# • ultracentrifuge 25000 €
# • table centrifuge 1200 €
# • freezer 2000 €
# • pipette 500 €
# • vortex 350 €
# • safe chemical cabinet 1100 €
stuff = {'ultracentrifuge': 25000,
         'table centrifuge': 1200,
         'freezer': 2000,
         'pipette': 500,
         'vortex': 350,
         'safe chemical cabinet': 1100
         } # expand the dictionary to the complete list 


# now change the script below to get the correct answer: 2350
z = stuff['ultracentrifuge']
print(z)
x = stuff['freezer'] 
y = stuff['vortex'] 
print("the two items freezer and vortex will cost together: {} euro".format(x + y))

# Question 4
# Given the molecular weights of Carbon (C; 12.011),
# Hydrogen (H; 1.0079), Oxygen (O, 15.999)
# Create a dictionary to store the molecular weights of the atoms
# with their single-letter abbreviation as key

atoms = {'C': 12.011,
         'H': 1.0079,
         'O': 15.999
         } # complete this

print("\n")

# use the dictionary to calculate the weight of glucose (C6H12O6)
c = atoms['C']
h = atoms['H']
o = atoms['O']
weight = 6*c+12*h+6*o
print("The atom weight of C6H12O6 is: {}".format(weight))


#Question 5
#print 3 to 0 and afterwards "Go!". Use a for loop with the function range()
for i in range(3,-1,-1):
    print(i)
print('Go!')
#Question 6
#Loop from 10 till 30. Print all numbers except the numbers 14 and 27
wrong = [14,27]
for i in range(10,30):
    if i not in wrong:
        print(i)
