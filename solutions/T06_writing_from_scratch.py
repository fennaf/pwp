#!/usr/bin/env python3
"""
some description

    usage:
 
"""

# METADATA VARIABLES [change these where necessary]
__author__ = "your name"
__status__ = "Template"
__version__ = "2017.d1.v1"


# IMPORT
import sys

# GOBALS

# FUNCTIONS

def count_cg(sequence):
    """count cg percentage"""
    gc = sequence.count('G') + sequence.count('C')
    return (gc / len(sequence))

def split_file(file):
    header = ''
    seq = ''
    with open(file, 'r') as f:
        for line in f:
            line = line.strip()
            if line.startswith(">"):
                header = line
            else:
                seq += line
    return(header, seq)


def main(args):
    with open('log.txt', 'w') as o:
        filenames = args[1:]
        for file in filenames:
            [header, sequence] = split_file(file)
            o.write(header + '\n')
            o.write('CG percentage: {:.2%}'.format(count_cg(sequence)))
            o.write('\n')
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
