
with open('example.txt', 'w') as f:
    f.write('{1}{1}{1}{0}{linebreak}'.format('A', 'G', linebreak='\n'))
    f.write('{1}{0}{1}{0}{linebreak}'.format('A', 'G', linebreak='\n'))


with open('example.txt', 'r') as f:
    i = 1
    for line in f:
        print('{counter:02d} {row}'.format(counter = i, row = line))
        i += 1

    
