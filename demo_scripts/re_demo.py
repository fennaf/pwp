import re
dna = "ATCGCGGCTTCAC"

m =  re.search(r"GC[ATGC]GC", dna)
print(m.group())


m = re.findall(r"[GC]",dna)
print(*m)


dna = "CGCTCNTAGATGCGCRATGACTGCAYTGC" 
matches = re.finditer(r"[^ATCG]", dna) 
for m in matches: 
    base = m.group() 
    pos  = m.start() 
    print(base + " found at position " + str(pos))
