#!/usr/bin/env python3
""" get username from prompt and print welcome message """


#get the username from a prompt
username = input("Type your name here: ")
username = username.upper()

#list of allowed users
user1 = "ANDY"
user2 = "KAYLEIGH"

#check if the user belongs 
#to the list of allowed users

if username == user1:
    print("Access granted")
elif username == user2:
    print("Welcome")
else:
    print("Access denied")
