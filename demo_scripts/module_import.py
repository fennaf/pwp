#!/usr/bin/env python3

""" reverse complementary strand """

__author__ = "Fenna Feenstra"


# code
# Imports

import sys
import DNA

# contantes or globals
# functions
# main


def main(argv):
    if len(argv) < 2:
        print("please provide a sequence")
        return 0

    seq = argv[1]
    print(seq)
    rev_seq = DNA.reverse(seq)
    print(rev_seq)
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))


