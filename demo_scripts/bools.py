"""exercise: try each line in the python interpreter and try explain the answer"""
True or True
True or False
True and False
not(True and False)
not True and False
True and False or True
True and False or False
'yes' == 'yes'
'yes' == 'yes' and 'no' == 'no'
'yes' == 'yes' and not 'no' == 'no'
'yes' == 'yes' or not 'no' == 'no'
not('yes' == 'yes' and 'no' != 'no')
5>4
5>4 or 2>3
5>4 and 2>3
4>5 or 'yes' != 'no'
4>2 and not 'yes' != 'no'
4>2 and not not 'yes' != 'no'
4>5 and 3<6 or not('yes'=='yes' and 'no' !='no')
