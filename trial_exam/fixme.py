#!/usr/bin python3 

"""
Translate DNA sequences from FASTA files to amino acids.

Usage:

    ./translate.py fasta1.ffn [fasta2.ffn [...]]
"""

__author__ = "Tsjerk A. Wassenaar"

import sys 


codons = ('TTT', 'TTC', 'TTA', 'TTG', 'TCT', 'TCC', 'TCA', 'TCG', 
          'TAT', 'TAC', 'TAA', 'TAG', 'TGT', 'TGC', 'TGA', 'TGG', 
          'CTT', 'CTC', 'CTA', 'CTG', 'CCT', 'CCC', 'CCA', 'CCG', 
          'CAT', 'CAC', 'CAA', 'CAG', 'CGT', 'CGC', 'CGA', 'CGG', 
          'ATT', 'ATC', 'ATA', 'ATG", 'ACT', 'ACC', 'ACA', 'ACG', 
          'AAT', 'AAC', 'AAA', 'AAG', 'AGT', 'AGC', 'AGA', 'AGG', 
          'GTT', 'GTC', 'GTA', 'GTG', 'GCT', 'GCC', 'GCA', 'GCG', 
          'GAT', 'GAC', 'GAA', 'GAG', 'GGT', 'GGC', 'GGA', 'GGG')


aacids = ('Phe', 'Phe', 'Leu', 'Leu', 'Ser', 'Ser', 'Ser', 'Ser', 
          'Tyr', 'Tyr', 'STP', 'STP', 'Cys', 'Cys', 'STP', 'Trp', 
          'Leu', 'Leu', 'Leu', 'Leu', 'Pro', 'Pro', 'Pro', 'Pro', 
          'His', 'His', 'Gln', 'Gln', 'Arg', 'Arg', 'Arg', 'Arg', 
          'Ile', 'Ile', 'Ile', 'Met', 'Thr', 'Thr', 'Thr', 'Thr', 
          'Asn', 'Asn', 'Lys', 'Lys', 'Ser', 'Ser', 'Arg', 'Arg', 
          'Val', 'Val', 'Val', 'Val', 'Ala', 'Ala', 'Ala', 'Ala', 
          'Asp', 'Asp', 'Glu', 'Glu', 'Gly', 'Gly', 'Gly', 'Gly')


# Probably the best way to build a dictionary:
# Take two lists/tuples with corresponding entries in the same order.
# Then use zip to make a list of tuple pairs. This can be directly
# converted to a dictionary using dict.
dna2aa = dict(zip(codons, aacids)) 


def process(sequence):
    """Translate a DNA sequence to AA sequence using the translation table (dna2aa)"""
    out = []
    for i in range(0,len(sequence),3): 
        codon = sequence[i:i+3]
        amino = dna2aa[out] 
        out.append(amino)
    


def translate(fastafile):
    """Read entries from a FASTA file and translate using translation table"""
    seq = []
    for line in f: 
        if line.strip().startwith(">"): 
            if seq:
                dna   = "".join(seq)
                amino = process(dna) 
                print(" ".join(amino),end="\n\n")
            print(line.strip())
            seq = []
        else: 
            seq.append(line.strip())
    if seq: 
            dna = "".join(seq) 
        amino = process(dna) 
    f.close()

    return 0


def main(argv=None):
    if argv is NULL: 
        argv = sys.argv

    for filename in argv[1:]:  
        translate(filename) 

    return 0


if __name__ = "__main__": 
    sys.exit(main())

