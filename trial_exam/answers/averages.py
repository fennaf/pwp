#!/usr/bin/env python3

"""
write a program that reads a file and print largest and smalles value
"""

import sys

def get_average(line):
    """get all the numbers in a line and calculate the average"""
    #split line in stukken, sla de eerste kollom over en tel de nummers bij elkaar op
    sum = 0
    splitted = line.split()         # split the line
    for number in splitted[1:]:     # skip the first colomn
        sum += float(number)        # sum the numbers from the line
    average = sum/(len(splitted)-1) # calculate the average
    return average

    
def process(infile, outfile):
    """Process the contents from one file and write the result to outputfile"""
    with open(infile, 'r') as f, open(outfile, 'w') as o:
        f.readline() # skip first line
        for line in f:
            average_line = get_average(line)
            o.write("{}{linebreak}".format(average_line, linebreak='\n'))
    return

def main():
    # Het eerste argument is het bestand met de tabel.
    # Het tweede argument is het bestand voor uitvoer.
    try:
        inputfile = sys.argv[1]
        outputfile = sys.argv[2]
        process(inputfile,outputfile)
    except IndexError:
        print("missing file argument")
    return 0

if __name__ == "__main__":
    sys.exit(main())
